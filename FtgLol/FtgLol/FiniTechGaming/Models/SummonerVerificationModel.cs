﻿using System.Collections.Generic;

namespace FtgLol.FiniTechGaming.Models
{
    public class SummonerVerificationModel
    {
        /// <summary>
        /// Code to verify League of Legends account.
        /// </summary>
        public string VerificationCode { get; set; }

        /// <summary>
        /// List of all League of Legends regions.
        /// </summary>
        public List<string> Regions { get; set; }
    }
}