﻿namespace FtgLol.FiniTechGaming.Controllers
{
    public interface IFtgSummonerVerificationController
    {
        bool VerifyAccount(string userName, string summonerName, string region);

        string VerifyAccount(string userName);
    }
}
