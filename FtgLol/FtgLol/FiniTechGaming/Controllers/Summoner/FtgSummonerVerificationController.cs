﻿using AutoMapper;
using FtgLol.Riot.Controllers;
using FtgLol.Data;
using FtgLol.FiniTechGaming.Data;
using FtgLol.Riot.Data;
using System;
using System.Diagnostics;
using System.Linq;
using FtgGamingLoL.Riot.Data;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace FtgLol.FiniTechGaming.Controllers
{
    public class FtgSummonerVerificationController : IFtgSummonerVerificationController
    {
        readonly FtgContext FtgContext;
        readonly ApplicationDbContext AppContext;

        #region Constructors
        public FtgSummonerVerificationController(ApplicationDbContext appContext, FtgContext ftgContext)
        {
            AppContext = appContext;
            FtgContext = ftgContext;
        }
        #endregion

        /// <summary>
        /// Used to verify a summoner account for League of Legends.
        /// Use only after VerifyAccountStart
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="summonerName"></param>
        /// <param name="region"></param>
        /// <returns></returns>
        public bool VerifyAccount(string userId, string summonerName, string region)
        {
            try
            {
                var endpointList = new RegionalEndpointList().GetRegionalEndpoints();
                RegionalEndpoint regionalEndpoint = endpointList.Single(endpoint => endpoint.ServicePlatform.ToLower() == region.ToLower());

                // Find the summoner information from Riot.
                RiotSummonerController summonerController = new RiotSummonerController(regionalEndpoint);
                // This method is not returning the correct response!
                RiotSummoner summonerDTO = summonerController.GetResponse(summonerName);

                var user = AppContext.Users.Find(userId);

                RiotThirdPartyCodeController thirdPartyVerificationController = new RiotThirdPartyCodeController(regionalEndpoint);
                string thirdPartyCode = thirdPartyVerificationController.GetResponse(summonerDTO.id);
                thirdPartyCode = thirdPartyCode.Replace("\"", "");

                var summonerCode = FtgContext.SummonerVerificationCode.Single(code => code.UserName == userId);


                bool isVerified = summonerCode.VerificationCode == thirdPartyCode;

                if (isVerified)
                {
                    FtgSummoner summoner = Mapper.Map<FtgSummoner>(summonerDTO);
                    summoner.Region = regionalEndpoint.ServicePlatform;

                    FtgUser ftgUser = new FtgUser()
                    {
                        UserId = user.Id,
                        Summoners = new List<FtgSummoner>()
                        {
                            summoner,
                        },
                    };
                                                           
                    FtgContext.SaveChanges();
                    return true;
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return false;
        }

        /// <summary>
        /// Used to start the summoner verification process.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string VerifyAccount(string userName)
        {
            IdentityUser currentUser = AppContext.Users.Single(user => user.UserName == userName);


            string verification = "InvalidCode";

            if (FtgContext.SummonerVerificationCode.Any(code => code.UserName == userName))
            {
                var code = FtgContext.SummonerVerificationCode.Find(userName);
                verification = code.VerificationCode;
            }
            else
            {
                verification = Guid.NewGuid().ToString();
                FtgContext.SummonerVerificationCode.Add(new FtgSummonerVerificationCode()
                {
                    UserName = userName,
                    VerificationCode = verification,
                });
            }

            FtgContext.SaveChanges();


            return verification;
        }
    }
}
