﻿using FtgLol.FiniTechGaming.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtgLol.FiniTechGaming.Controllers
{
    public class FtgShardController : IFtgShardController
    {
        private readonly FtgContext Context;
        public FtgShardController(FtgContext context)
        {
            Context = context;
        }

        public FtgShardController() { }

        public async Task<List<string>> GetRegionList()
        {
            return await Context.Shards.Select(shard => shard.RegionTag).ToListAsync();            
        }
    }
}
