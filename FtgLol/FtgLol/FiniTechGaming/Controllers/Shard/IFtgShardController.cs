﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtgLol.FiniTechGaming.Controllers
{
    public interface IFtgShardController
    {
        Task<List<string>> GetRegionList();
    }
}
