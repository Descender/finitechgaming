﻿using Microsoft.EntityFrameworkCore;
namespace FtgLol.FiniTechGaming.Data
{
    public class FtgContext : DbContext
    {
        #region Constructors
        public FtgContext() { }

        public FtgContext(DbContextOptions<FtgContext> options) : base(options) { }
        #endregion

        #region Champion
        public DbSet<FtgChampion> Champions { get; set; }
        #endregion

        //#region Item
        //public DbSet<Block> Blocks { get; set; }
        //public DbSet<BlockItem> BlockItems { get; set; }
        //public DbSet<Group> Groups { get; set; }
        public DbSet<FtgItem> Items { get; set; }
        //public DbSet<ItemTree> ItemTrees { get; set; }
        //#endregion

        //#region Mastery
        //public DbSet<Mastery> Masteries { get; set; }
        //public DbSet<MasteryTree> MasteryTrees { get; set; }
        //public DbSet<MasteryTreeItem> MasteryTreeItems { get; set; }
        //#endregion

        #region Match
        //public DbSet<Match> Matches { get; set; }
        //public DbSet<MatchReference> MatchReferences { get; set; }
        //public DbSet<Participant> Participants { get; set; }
        //public DbSet<ParticipantIdentity> ParticipantIdentities { get; set; }
        //public DbSet<ParticipantRune> ParticipantRunes { get; set; }
        //public DbSet<TeamBans> TeamBans { get; set; }
        //public DbSet<TeamStats> TeamStats { get; set; }
        #endregion

        //#region Runes
        //public DbSet<ReforgedRunePath> ReforgedRunePaths { get; set; }
        //public DbSet<ReforgedRune> ReforgedRunes { get; set; }
        //public DbSet<ReforgedRuneSlot> ReforgedRuneSlots { get; set; }
        //public DbSet<Rune> Runes { get; set; }
        //#endregion

        #region Shard
        public DbSet<FtgIncident> Incidents { get; set; }
        public DbSet<FtgLocales> Locales { get; set; }
        public DbSet<FtgMessage> Messages { get; set; }
        public DbSet<FtgService> Services { get; set; }
        public DbSet<FtgShard> Shards { get; set; }
        public DbSet<FtgTranslation> Translations { get; set; }
        #endregion

        #region Summoner
        //public DbSet<LevelTip> LevelTip { get; set; }        
        public DbSet<FtgSummonerVerificationCode> SummonerVerificationCode { get; set; }
        //public DbSet<SummonerSpell> SummonerSpells { get; set; }
        //public DbSet<SummonerSpellCooldown> SummonerSpellCooldowns { get; set; }
        //public DbSet<SummonerSpellCosts> SummonerSpellCosts { get; set; }
        //public DbSet<SummonerSpellEffectBurns> SummonerSpellEffectBurns { get; set; }
        //public DbSet<SummonerSpellEffectList> SummonerSpellEffectLists { get; set; }
        //public DbSet<SummonerSpellEffects> SummonerSpellEffects { get; set; }
        //public DbSet<SummonerSpellModes> SummonerSpellModes { get; set; }
        #endregion

        #region User
        public DbSet<FtgUser> FtgUsers { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Champion
            modelBuilder.Entity<FtgChampion>(entity =>
            {
                entity
                .HasKey(champion => new { champion.Id, champion.LocaleName, champion.Version });

                entity
                .HasMany(champion => champion.Tags)
                .WithOne();

                entity
                .Property(champion => champion.LocaleName);
                
            });

            modelBuilder.Entity<FtgChampionTag>(entity =>
            {
                entity
                .HasKey(tag => tag.Tag);
            });
            #endregion

            #region Item
            modelBuilder.Entity<FtgItem>(entity =>
            {
                entity
                .HasKey(item => new { item.Id, item.LocaleName, item.Version });

                entity
                .HasMany(item => item.Tags)
                .WithOne();

                entity
                .HasOne(item => item.Locale)
                .WithMany()
                .HasForeignKey(item => item.LocaleName);
            });

            modelBuilder.Entity<FtgItemTag>(entity =>
            {
                entity
                .HasKey(tag => tag.Tag);
            });
            #endregion

            #region Shard
            modelBuilder.Entity<FtgShard>(entity =>
        {
            entity
            .HasKey(shard => shard.RegionTag);

            entity
            .HasMany(shard => shard.Locales)
            .WithOne(locale => locale.Shard)
            .HasForeignKey(shard => shard.HostNameString);

            entity
            .HasIndex(index => index.HostName)
            .IsUnique();

            entity
            .HasMany(shard => shard.Services)
            .WithOne(locale => locale.ShardStatus)
            .HasForeignKey(locale => locale.HostName);
        });

            modelBuilder.Entity<FtgLocales>(entity =>
            {
                entity
                .HasKey(locale => locale.Name);
            });

            modelBuilder.Entity<FtgService>(entity =>
            {
                entity
                .HasKey(service => new { service.Name, service.HostName });

                entity
                .HasMany(service => service.Incidents)
                .WithOne(incident => incident.Service)
                .HasForeignKey(incident => new { incident.ServiceName, incident.ServiceHostName });
            });

            modelBuilder.Entity<FtgIncident>(entity =>
            {
                entity
                .HasKey(key => key.Id);

                entity
                .HasMany(incident => incident.Updates)
                .WithOne(update => update.Incident)
                .HasForeignKey(incident => incident.IncidentId);
            });

            modelBuilder.Entity<FtgMessage>(entity =>
            {
                entity
                .HasKey(key => key.Id);

                entity
                .HasMany(message => message.Translations)
                .WithOne(translation => translation.Message)
                .HasForeignKey(message => message.MessageId);
            });

            modelBuilder.Entity<FtgTranslation>(entity =>
            {
                entity
                .HasKey(translation => new { translation.LocaleName, translation.MessageId });
            });
            #endregion

            #region Summoner / User
            modelBuilder.Entity<FtgUser>()
                .HasKey(user => user.UserId);

            modelBuilder.Entity<FtgSummoner>()
                .HasKey(summoner => summoner.AccountId);

            modelBuilder.Entity<FtgSummonerVerificationCode>()
                .HasKey(code => code.UserName);
            #endregion
        }
    }
}
