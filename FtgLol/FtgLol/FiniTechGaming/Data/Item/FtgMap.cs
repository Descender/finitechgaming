﻿namespace FtgLol.FiniTechGaming.Data
{
    public class FtgMap
    {
        public int Id { get; set; }
        public bool IsAvailable { get; set; }
        public string ItemId { get; set; }
        public string ItemLocale { get; set; }
        public string Version { get; set; }
        public FtgItem Item { get; set; }
    }
}