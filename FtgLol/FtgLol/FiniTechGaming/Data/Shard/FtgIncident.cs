﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FtgLol.FiniTechGaming.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FtgIncident
    {
        /// <summary>
        /// Primary Key.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Created { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<FtgMessage> Updates { get; set; }

        public string ServiceHostName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual FtgService Service { get; set; }
    }
}
