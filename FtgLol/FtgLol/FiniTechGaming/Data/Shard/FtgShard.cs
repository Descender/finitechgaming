﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace FtgLol.FiniTechGaming.Data
{
    /// <summary>
    /// Shard status for a given region.
    /// </summary>
    public class FtgShard
    {
        #region Properties

        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RegionTag { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<FtgService> Services { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Slug { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<FtgLocales> Locales { get; set; } = new List<FtgLocales>();

        public string ServiceRegion { get; set; }

        public string ServicePlatform { get; set; }

        public string Host { get; set; }

        public string Version { get; set; }
        #endregion
    }
}
