﻿namespace FtgLol.FiniTechGaming.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FtgTranslation
    {
        /// <summary>
        /// 
        /// </summary>
        public string LocaleName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UpdatedAt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MessageId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual FtgMessage Message { get; set; }
    }
}
