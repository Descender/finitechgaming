﻿using System.Collections.Generic;

namespace FtgLol.FiniTechGaming.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FtgService
    {
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<FtgIncident> Incidents { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Slug { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual FtgShard ShardStatus { get; set; }
    }
}
