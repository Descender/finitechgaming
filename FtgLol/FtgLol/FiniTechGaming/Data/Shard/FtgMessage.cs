﻿using System.Collections.Generic;

namespace FtgLol.FiniTechGaming.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FtgMessage
    {
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Severity { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<FtgTranslation> Translations { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Created { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Updated { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int IncidentId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual FtgIncident Incident { get; set; }
    }
}
