﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FtgLol.FiniTechGaming.Data
{
    /// <summary>
    /// 
    /// </summary>
    public class FtgLocales
    {
        /// <summary>
        /// Locales name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HostNameString { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual FtgShard Shard { get; set; }
    }
}
