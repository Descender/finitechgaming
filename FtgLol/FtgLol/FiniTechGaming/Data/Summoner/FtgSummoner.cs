﻿namespace FtgLol.FiniTechGaming.Data
{
    public class FtgSummoner
    {
        #region Class Properties
        /// <summary>
        /// Summoner ID.
        /// </summary>
        public string SummonerId { get; set; }

        /// <summary>
        /// Account ID.
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Puuid.
        /// </summary>
        public string Puuid { get; set; }

        /// <summary>
        /// Summoner name
        /// </summary>
        public string SummonerName { get; set; }

        /// <summary>
        /// Summoner level associated with the summoner
        /// </summary>
        public long? SummonerLevel { get; set; }

        /// <summary>
        /// ID of the summoner icon associated with the summoner
        /// </summary>
        public int? ProfileIconId { get; set; }

        /// <summary>
        /// Date summoner was last modified specified as epoch milliseconds. The following events will update this timestamp:
        /// profile icon change, playing the tutorial or advanced tutorial, finishing a game, summoner name change
        /// </summary>
        public long? RevisionDate { get; set; }

        /// <summary>
        /// Date summoner was last modified specified as epoch milliseconds. The following events will update this timestamp:
        /// profile icon change, playing the tutorial or advanced tutorial, finishing a game, summoner name change
        /// </summary>
        public string Region { get; set; }

        //public FtgImage ProfileImage { get; set; }
        #endregion
    }
}
