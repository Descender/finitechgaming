﻿using System.Collections.Generic;

namespace FtgLol.FiniTechGaming.Data
{
    public class FtgUser
    {
        public string UserId { get; set; }

        public List<FtgSummoner> Summoners { get; set; }
    }
}
