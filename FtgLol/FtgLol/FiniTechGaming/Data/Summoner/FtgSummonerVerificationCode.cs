﻿namespace FtgLol.FiniTechGaming.Data
{
    public class FtgSummonerVerificationCode 
    {
        public string UserName { get; set; }

        public string VerificationCode { get; set; }
    }
}