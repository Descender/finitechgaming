﻿using FtgLol.FiniTechGaming.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FtgLol.FiniTechGaming.Controllers
{
    public class SummonerVerificationController : Controller
    {
        private readonly IFtgSummonerVerificationController VerificationController;

        public SummonerVerificationController(IFtgSummonerVerificationController verificationController)
        {
            VerificationController = verificationController;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult>RegisterSummoner([FromServices]IFtgShardController shardController)
        {
            var userName = User.FindFirst(ClaimTypes.Name).Value;
            string verificationCode = VerificationController.VerifyAccount(userName);

            SummonerVerificationModel model = new SummonerVerificationModel()
            {
                VerificationCode = verificationCode,
                Regions = await shardController.GetRegionList(),
            };

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult VerifySummoner(string summonerName, string region)
        {
            var userName = User.FindFirst(ClaimTypes.Name).Value;

            bool isSuccess = VerificationController.VerifyAccount(userName, summonerName, region);

            if (isSuccess)
            {
                return View("RegisterComplete");
            }
            else
            {
                return RedirectToAction("RegisterSummoner");
            }
        }
    }
}
