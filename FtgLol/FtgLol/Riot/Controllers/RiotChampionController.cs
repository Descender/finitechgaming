﻿using FtgLol.FiniTechGaming.Data;
using FtgLol.Riot.Data;
using RestSharp;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class RiotChampionController : AbstractRiotRequest
    {
        private const string CHAMPION_JSON = "champion.json";
        private const string PLATFORM = "platform";
        private const string CHAMPION_ROTATIONS = "champion-rotations";

        public RiotChampionController(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }
        public RiotChampionController(FtgShard shard) : base(shard) { }

        #region Requests
        public RiotChampionData GetResponse()
        {
            RestClient client = GetRiotRestClient();
            RestRequest request = GetRiotDataDragonRequest(null, CHAMPION_JSON);
            IRestResponse<RiotChampionData> dto = client.Execute<RiotChampionData>(request);
            return dto.Data;
        }

        public RiotFreeToPlayChampions GetResponseFtp()
        {
            RestRequest request = GetRiotRequestV3(PLATFORM, CHAMPION_ROTATIONS);
            IRestResponse<RiotFreeToPlayChampions> dto = client.Execute<RiotFreeToPlayChampions>(request);
            return dto.Data;
        }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}