﻿using AutoMapper;
using FtgLol.FiniTechGaming.Data;
using FtgLol.Riot.Controllers;
using FtgLol.Riot.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FtgLol.FiniTechGaming.Extractor
{
    public class RiotDataUpdater : IHostedService
    {
        #region Private Fields
        private readonly IServiceScopeFactory ServiceScopeFactory;

        private readonly ILogger Logger;

        private FtgContext Context;
        #endregion

        #region Constructors
        /// <summary>
        /// Class with a custom update interval.
        /// </summary>
        /// <param name="serviceScopeFactory"></param>
        public RiotDataUpdater(IServiceScopeFactory serviceScopeFactory, ILoggerFactory loggerFactory)//IFiniTechGamingContext context)
        {
            ServiceScopeFactory = serviceScopeFactory;
            Logger = loggerFactory.CreateLogger<RiotDataUpdater>();
        }
        #endregion

        #region Hosted Service Methods
        public Task StartAsync(CancellationToken cancellationToken = default)
        {
            var RiotUpdateTimer = new Timer(UpdateLeagueOfLegendsData, null, TimeSpan.Zero, TimeSpan.FromHours(12));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken = default)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Update League of Legends data
        /// <summary>
        /// Main Riot updater method.
        /// Updates League of Legends data from Riot servers.
        /// </summary>
        private async void UpdateLeagueOfLegendsData(object state)
        {
            try
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    using (var context = scope.ServiceProvider.GetService<FtgContext>())
                    {
                        Context = context;
                        Logger.LogInformation("Gathering endpoint list.");
                        RegionalEndpointList regionalEndpointList = new RegionalEndpointList();
                        List<RegionalEndpoint> regionalEndpoints = regionalEndpointList.GetRegionalEndpoints();
                        Logger.LogInformation("Gathering shards.");

                        foreach (var endpoint in regionalEndpoints)
                        {
                            await UpdateRiotData(endpoint);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        /// <summary>
        /// Method used for different endpoints.
        /// </summary>
        /// <param name="regionalEndpoint">Riot regional endpoint.</param>
        private async Task UpdateRiotData(RegionalEndpoint regionalEndpoint)
        {
            try
            {
                FtgShard shard = new FtgShard();

                // Save Shard Data
                try
                {
                    Logger.LogDebug($"Updating Regional Endpoint {regionalEndpoint.Host}.");
                    shard = UpdateRiotShardData(regionalEndpoint);
                    Logger.LogDebug($"Shard save success for Regional Endpoint {regionalEndpoint.Host}.");
                }
                catch (Exception ex)
                {
                    Logger.LogError($"Shard save failure: {ex.Message}");
                }

                // If something went wrong getting the shard, exit this method.
                if (shard.Host == null)
                {
                    Logger.LogError($"Shard is null. Exiting UpdateRiotData. Regional Endpoint {regionalEndpoint.Host}.");
                    return;
                }

                foreach (var locale in shard.Locales)
                {
                    // Save Champion Data.                    
                    try
                    {
                        await UpdateChampionData(shard, locale);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError($"Champion save failure: {ex.Message}");
                    }

                    try
                    {
                        await UpdateItemData(shard, locale);
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError($"Item save failure: {ex.Message}");

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        private FtgShard UpdateRiotShardData(RegionalEndpoint regionalEndpoint)
        {
            //Get Shard Data first
            RiotShardStatusController riotShardStatusController = new RiotShardStatusController(regionalEndpoint);
            RiotShardStatus riotShard = riotShardStatusController.GetResponse();
            FtgShard ftgShard = Mapper.Map<FtgShard>(riotShard);

            RiotVersionController riotVersionController = new RiotVersionController(regionalEndpoint);
            ftgShard.Version = riotVersionController.GetResponse() ?? "9.10.1";
            ftgShard.ServiceRegion = regionalEndpoint.ServiceRegion;
            ftgShard.ServicePlatform = regionalEndpoint.ServicePlatform;
            ftgShard.Host = regionalEndpoint.Host;

            if (Context.Shards.Any(shard => shard.RegionTag == shard.RegionTag &&
            shard.Version == shard.Version))
            {
                var dbShard = Context.Shards
                    .Single(shard => shard.RegionTag == shard.RegionTag &&
                    shard.Version == shard.Version);
                Context.Entry(dbShard).CurrentValues.SetValues(ftgShard);
            }
            else
            {
                Context.Shards.Add(ftgShard);
            }
            Context.SaveChanges();
            return Context.Shards
                    .Where(shard => shard.RegionTag == ftgShard.RegionTag &&
                    shard.Version == shard.Version)
                    .Include(shard => shard.Locales)
                    .Single();
        }



        private async Task UpdateChampionData(FtgShard shard, FtgLocales locales)
        {
            RiotChampionController championController = new RiotChampionController(shard);
            RiotChampionData championData = championController.GetResponse();

            foreach (var riotChamp in championData.data)
            {
                FtgChampion ftgChampion = Mapper.Map<FtgChampion>(riotChamp.Value);
                ftgChampion.Locales = locales;
                ftgChampion.LocaleName = locales.Name;
                ftgChampion.Version = shard.Version;

                if (Context.Champions.Any(champ => champ.Id == ftgChampion.Id &&
                champ.Version == ftgChampion.Version &&
                champ.LocaleName == ftgChampion.Locales.Name))
                {
                    var dbChampion = Context.Champions
                        .Single(champ => champ.Id == ftgChampion.Id &&
                        champ.Version == ftgChampion.Version &&
                        champ.LocaleName == ftgChampion.Locales.Name);
                    Context.Entry(dbChampion).CurrentValues.SetValues(ftgChampion);
                }
                else
                {
                    Context.Champions.Add(ftgChampion);
                }
            }
            await Context.SaveChangesAsync();
        }


        public async Task UpdateItemData(FtgShard shard, FtgLocales locales)
        {
            RiotItemController itemController = new RiotItemController(shard);
            RiotItemData riotItems = itemController.GetResponse();

            foreach (var riotItem in riotItems.data)
            {
                FtgItem ftgItem = Mapper.Map<FtgItem>(riotItem.Value);
                ftgItem.Id = riotItem.Key;
                ftgItem.LocaleName = locales.Name;
                ftgItem.Version = shard.Version;

                if (Context.Items.Any(item => item.Id == ftgItem.Id &&
                item.LocaleName == ftgItem.LocaleName &&
                item.Version == ftgItem.Version))
                {
                    var dbItem = Context.Items
                        .Single(item => item.Id == ftgItem.Id &&
                        item.Locale == ftgItem.Locale &&
                        item.Version == ftgItem.Version);
                    Context.Entry(dbItem).CurrentValues.SetValues(ftgItem);
                }
                else
                {
                    Context.Items.Add(ftgItem);
                }
            }
            await Context.SaveChangesAsync();
        }
    }
}
#endregion

