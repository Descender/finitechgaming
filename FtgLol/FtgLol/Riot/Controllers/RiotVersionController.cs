﻿using FtgLol.Riot.Data;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class RiotVersionController : AbstractRiotRequest
    {
        private const string VERSIONS_JSON = "versions.json";

        public RiotVersionController(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }

        #region Requests
        public string GetResponse()
        {
            RestClient client = GetRiotRestClient();
            RestRequest request = GetRiotDataDragonRequest(VERSIONS_JSON);
            IRestResponse<List<string>> dto = client.Execute<List<string>>(request);
            return dto.Data.First();
        }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}