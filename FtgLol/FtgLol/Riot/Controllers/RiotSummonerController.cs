﻿using FtgGamingLoL.Riot.Data;
using FtgLol.Riot.Data;
using RestSharp;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class RiotSummonerController : AbstractRiotRequest
    {
        #region Constants
        private readonly string BY_NAME = "by-name";
        private readonly string SUMMONER = "summoner";
        private readonly string SUMMONERS = "summoners";
        #endregion

        #region Constructors
        public RiotSummonerController(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }
        #endregion

        #region Requests
        /// <summary>
        /// Returns summoner data by summoner name.
        /// </summary>
        /// <param name="summonerName"></param>
        /// <returns></returns>
        public RiotSummoner GetResponse(string summonerName)
        {
            RestRequest request = GetRiotRequest(SUMMONER, SUMMONERS, BY_NAME, summonerName);
            IRestResponse<RiotSummoner> response = client.Execute<RiotSummoner>(request);
            var result = response.Data;
            return result;
        }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
