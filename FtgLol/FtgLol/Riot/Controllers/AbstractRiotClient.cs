﻿using FtgLol.FiniTechGaming.Data;
using FtgLol.Riot.Data;
using RestSharp;
using System;
using System.Linq;

namespace FtgLol.Riot.Controllers
{
    public abstract class AbstractRiotClient
    {
        #region Constants
        protected const string BASE_URL = "https://";
        protected const string DATA_DRAGON = "https://ddragon.leagueoflegends.com/";
        protected RestClient client;
        #endregion

        #region Properties
        protected RegionalEndpoint RegionalEndpoint;
        #endregion

        #region Constructors
        protected AbstractRiotClient(RegionalEndpoint regionalEndpoint)
        {
            RegionalEndpoint = regionalEndpoint;
            client = new RestClient()
            {
                BaseUrl = new Uri(BASE_URL + regionalEndpoint.Host),
            };
        }

        protected AbstractRiotClient(FtgShard ftgShard)
        {
            RegionalEndpoint = new RegionalEndpoint()
            {
                Host = ftgShard.Host,
                Locales = ftgShard.Locales.First().Name,
                ServicePlatform = ftgShard.ServicePlatform,
                ServiceRegion = ftgShard.ServiceRegion,
                Version = ftgShard.Version,
            };
            client = new RestClient()
            {
                BaseUrl = new Uri(BASE_URL + ftgShard.Host),
            };
        }
        #endregion

        #region Client
        /// <summary>
        /// Get Data Dragon Rest client.
        /// </summary>
        /// <returns>RestClient for Riot's Data dragon for League of Legends.</returns>
        protected RestClient GetRiotRestClient()
        {
            return new RestClient()
            {
                BaseUrl = new Uri(DATA_DRAGON),
            };
        }
        #endregion
    }
}
