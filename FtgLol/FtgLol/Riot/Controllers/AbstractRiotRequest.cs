﻿using FtgLol.FiniTechGaming.Data;
using FtgLol.Riot.Data;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public abstract class AbstractRiotRequest : AbstractRiotClient
    {
        #region Constants
        protected const string API = "API";
        protected const string API_LOWERCASE = "api";
        protected const string CDN = "cdn";
        protected const string DATA = "data";
        protected const string ENDPOINT = "ENDPOINT";
        protected const string KEY = "RGAPI-7b75b3b9-aeec-4848-a4fd-490dc72b470e";
        protected const string LOL = "lol";
        protected const string METHOD = "method";
        protected const string PARAMETER = "PARAMETER";
        private readonly string RIOT_REQUEST = $"{LOL}/{{{API}}}/{V4}/{{{ENDPOINT}}}/{{{PARAMETER}}}";
        private readonly string RIOT_REQUEST_BY_METHOD = $"{LOL}/{{{API}}}/{V4}/{{{ENDPOINT}}}/{{{METHOD}}}/{{{PARAMETER}}}";
        private readonly string RIOT_REQUEST_V3 = $"{LOL}/{{{API}}}/{V3}/{{{ENDPOINT}}}";
        protected const string SEARCHBY = "SEARCHBY";
        protected const string V3 = "v3";
        protected const string V4 = "v4";
        protected const string RIOT_REQUEST_REGEX = "^[0-9\\p{L} _\\.]+$";
        protected const string X_RIOT_TOKEN = "X-Riot-Token";
        #endregion

        #region Constructors
        public AbstractRiotRequest(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }
        public AbstractRiotRequest(FtgShard ftgShard) : base(ftgShard) { }
        #endregion               

        #region Request Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="api">Riot api.</param>
        /// <param name="endpoint">Riot endpoint.</param>
        /// <returns>RestRequest for Riot's League of Legends.</returns>
        protected RestRequest GetRiotRequest(string api, string endpoint)
        {
            string riotrequest = $"{LOL}/{{{API}}}/{V4}/{{{ENDPOINT}}}";
            RestRequest request = new RestRequest(riotrequest);
            request.AddUrlSegment(API, api);
            request.AddUrlSegment(ENDPOINT, endpoint);
            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        protected RestRequest GetRiotRequestV3(string api, string endpoint)
        {
            RestRequest request = new RestRequest(RIOT_REQUEST_V3, Method.GET);
            request.AddUrlSegment(API, api);
            request.AddUrlSegment(ENDPOINT, endpoint);
            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        protected RestRequest GetRiotRequest(string api, string endpoint, string parameter)
        {
            var request = new RestRequest(RIOT_REQUEST, Method.GET);
            request.AddUrlSegment(API, api);
            request.AddUrlSegment(ENDPOINT, endpoint);
            request.AddUrlSegment(PARAMETER, parameter);
            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        protected RestRequest GetRiotRequest(string api, string endpoint, string method, string parameter)
        {
            var request = new RestRequest(RIOT_REQUEST_BY_METHOD, Method.GET);
            request.AddUrlSegment(API, api);
            request.AddUrlSegment(ENDPOINT, endpoint);
            request.AddUrlSegment(METHOD, method);
            request.AddUrlSegment(PARAMETER, parameter);
            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        protected RestRequest GetRiotRequest(string api, string endpoint, string method, string parameter, List<Parameter> parameters)
        {
            var request = new RestRequest(RIOT_REQUEST_BY_METHOD, Method.GET);
            request.AddUrlSegment(API, api);
            request.AddUrlSegment(ENDPOINT, endpoint);
            request.AddUrlSegment(METHOD, method);
            request.AddUrlSegment(PARAMETER, parameter);

            parameters.ForEach(p => request.AddParameter(p));

            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        /// <summary>
        /// RestRequest for Riot's static data dragon.
        /// </summary>
        /// <param name="dragonName">Name of the static dragon.</param>
        /// <param name="objectName">Name of the object to receive.</param>
        /// <returns>RestRequest for Riot's data dragon.</returns>
        protected RestRequest GetRiotDataDragonRequest(string dragonName, string objectName)
        {
            string riotRequest = $"{CDN}/{RegionalEndpoint.Version}/{DATA}/{RegionalEndpoint.Locales}";

            if (dragonName != null)
            {
                riotRequest += $"/{dragonName}";
                riotRequest += $"/{objectName}";
            }
            else
            {
                riotRequest += $"/{objectName}";
            }

            RestRequest request = new RestRequest(riotRequest);
            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        /// <summary>
        /// RestRequest for Riot's static data dragon.
        /// </summary>
        /// <param name="dragonName">Name of the static dragon.</param>
        /// <param name="objectName">Name of the object to receive.</param>
        /// <returns>RestRequest for Riot's data dragon.</returns>
        protected RestRequest GetRiotDataDragonRequest(string objectName)
        {
            RestRequest request = new RestRequest($"/{API_LOWERCASE}/{objectName}");
            request.AddHeader(X_RIOT_TOKEN, KEY);
            return request;
        }

        protected abstract Task<T> GetResponseAsync<T>();
        #endregion
    }
}
