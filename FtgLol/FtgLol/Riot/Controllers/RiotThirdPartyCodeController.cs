﻿using FtgLol.Riot.Data;
using RestSharp;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class RiotThirdPartyCodeController : AbstractRiotRequest
    {
        #region Constants
        private const string BY_SUMMONER = "by-summoner";
        private const string PLATFORM = "platform";
        private const string THIRD_PARTY_CODE = "third-party-code";
        #endregion

        #region Constructors
        public RiotThirdPartyCodeController(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }
        #endregion

        #region Controller Methods
        public string GetResponse(string summonerID)
        {
            RestRequest restRequest = GetRiotRequest(PLATFORM, THIRD_PARTY_CODE, BY_SUMMONER, summonerID);
            IRestResponse restResponse = client.Execute(restRequest);
            return restResponse.Content;
        }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
