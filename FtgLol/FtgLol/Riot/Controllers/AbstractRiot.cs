﻿namespace FtgLol.Riot.Controllers
{
    public abstract class AbstractRiot
    {
        #region protected strings

        protected readonly string NAME = "name";

        protected readonly string STATUS = "status";
        protected readonly string SHARD_DATA = "shard-data";
        protected readonly string CHAMPIONS = "champions";
        protected readonly string STATIC_DATA = "static-data";
        protected readonly string BEGIN = "https://";

        protected readonly string SUMMONER = "summoner";
        protected readonly string SUMMONERS = "summoners";


        protected readonly string BY_ACCOUNT = "by-account";
        protected readonly string BY_NAME = "by-name";

        protected readonly string MATCH_LISTS = "matchlists";
        protected readonly string MATCH = "match";
        protected readonly string MATCHES = "matches";
        protected readonly string LOCALE = "LOCALE";
        protected readonly string TAGS = "tags";
        protected readonly string ALL = "all";
        protected readonly string CHAMPION_REQUEST = "champion.json";
        //protected readonly string VERSION = "VERSION";
        protected readonly string VERSION_API = "versions";


        protected readonly string ITEM_REQUEST = "item.json";
        protected readonly string LANGUAGE_STRING_REQUEST = "language.json";
        protected readonly string MAP_STRING_REQUEST = "map.json";
        protected readonly string MASTERY_JSON_FILE = "mastery.json";
        protected readonly string PROFILE_ICON_JSON_FILE = "profileicon.json";
        protected readonly object JSON_FILE = ".json";
        protected readonly string REFORGED_RUNE_PATH_JSON_FILE = "????";
        protected readonly string REFORGED_RUNE_JSON_FILE = "runesReforged.json";
        protected readonly string RUNE_JSON_FILE = "rune.json";
        protected readonly string SUMMONER_SPELL_JSON_FILE = "summoner.json";
        //protected readonly string API_KEY = "?api_key=";
        protected readonly string FREE_TO_PLAY = "freeToPlay";
        #endregion
    }
}
