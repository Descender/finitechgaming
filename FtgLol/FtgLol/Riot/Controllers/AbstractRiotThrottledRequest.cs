﻿using FtgLol.Riot.Data;
using RestSharp;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class AbstractRiotThrottledRequest<RequestType, ResponseType> : AbstractRiotRequest
    {
        protected static BlockingCollection<RequestType> Requests = new BlockingCollection<RequestType>();
        protected static ConcurrentDictionary<RequestType, ResponseType> Responses = new ConcurrentDictionary<RequestType, ResponseType>();

        public AbstractRiotThrottledRequest(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new NotImplementedException();
        }

        public bool AddRequest(RequestType request)
        {
            return Requests.TryAdd(request);
        }

        public bool TryTake(RequestType request, out ResponseType response)
        {
            return Responses.TryGetValue(request, out response);
        }

        protected virtual void Process() { }

        protected virtual RestRequest GetRiotRequest(string id) { return null; }
    }
}