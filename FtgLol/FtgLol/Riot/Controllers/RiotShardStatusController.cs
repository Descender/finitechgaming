﻿using FtgLol.Riot.Data;
using RestSharp;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class RiotShardStatusController : AbstractRiotRequest
    {
        #region Constants
        private readonly string STATUS = "status";
        private readonly string SHARD_DATA = "shard-data";
        #endregion

        #region Constructors
        public RiotShardStatusController(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint) { }
        #endregion

        #region Requests
        /// <summary>
        /// Returns summoner data by summoner name.
        /// </summary>
        /// <returns></returns>
        public RiotShardStatus GetResponse()
        {
            RestRequest request = GetRiotRequestV3(STATUS, SHARD_DATA);
            IRestResponse<RiotShardStatus> response = client.Execute<RiotShardStatus>(request);
            var result = response.Data;
            return result;
        }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}