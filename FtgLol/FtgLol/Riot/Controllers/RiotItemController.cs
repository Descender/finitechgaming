﻿using FtgLol.FiniTechGaming.Data;
using FtgLol.Riot.Data;
using RestSharp;
using System;
using System.Threading.Tasks;

namespace FtgLol.Riot.Controllers
{
    public class RiotItemController : AbstractRiotRequest
    {
        private const string ITEM_JSON = "item.json";

        public RiotItemController(RegionalEndpoint regionalEndpoint) : base(regionalEndpoint){}

        public RiotItemController(FtgShard ftgShard) : base(ftgShard) { }

        public RiotItemData GetResponse()
        {
            RestClient client = GetRiotRestClient();
            RestRequest request = GetRiotDataDragonRequest(null, ITEM_JSON);
            IRestResponse<RiotItemData> dto = client.Execute<RiotItemData>(request);
            return dto.Data;
        }

        protected override Task<T> GetResponseAsync<T>()
        {
            throw new NotImplementedException();
        }
    }
}
