﻿using System.Collections.Generic;

namespace FtgLol.Riot.Data
{
    public class RiotFreeToPlayChampions
    {
        public List<int> freeChampionIds { get; set; }
        public List<int> freeChampionIdsForNewPlayers { get; set; }
        public int maxNewPlayerLevel { get; set; }
    }
}