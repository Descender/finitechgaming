﻿using System;
using System.Collections.Generic;

namespace FtgLol.Riot.Data
{
    public class RiotShardStatus
    {
        public string name { get; set; }
        public string region_tag { get; set; }
        public string hostname { get; set; }
        public List<RiotService> services { get; set; }
        public string slug { get; set; }
        public List<string> locales { get; set; }
    }

    public class RiotService
    {
        public string status { get; set; }
        public List<RiotIncident> incidents { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
    }

    public class RiotIncident
    {
        public bool active { get; set; }
        public DateTime created_at { get; set; }
        public int id { get; set; }
        public List<RiotMessage> updates { get; set; }
    }

    public class RiotMessage
    {
        public string severity { get; set; }
        public string author { get; set; }
        public DateTime created_at { get; set; }
        public List<RiotTranslation> translations { get; set; }
        public DateTime updated_at { get; set; }
        public string content { get; set; }
        public string id { get; set; }
    }

    public class RiotTranslation
    {
        public string locale { get; set; }
        public string content { get; set; }
        public string updated_at { get; set; }

    }
}