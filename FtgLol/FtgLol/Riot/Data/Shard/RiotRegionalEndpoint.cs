﻿using System.Collections.Generic;

namespace FtgLol.Riot.Data
{
    /// <summary>
    /// Regional Endpoint Data
    /// </summary>
    public class RegionalEndpoint
    {
        public string ServiceRegion { get; set; }
        public string ServicePlatform { get; set; }
        public string Host { get; set; }
        public string Locales { get; set; }
        public string Version { get; set; }
    }
}
