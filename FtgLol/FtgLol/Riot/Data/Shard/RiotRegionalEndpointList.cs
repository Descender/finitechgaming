﻿using System.Collections.Generic;
using System.Linq;
namespace FtgLol.Riot.Data
{
    /// <summary>
    /// List of all Regional Endpoints.
    /// Hard coded.
    /// </summary>
    public class RegionalEndpointList
    {
        #region Constants
        private readonly string RiotSuffix = ".api.riotgames.com";
        #endregion

        #region Private Fields
        /// <summary>
        /// List of Riot's Endpoints.
        /// </summary>
        private List<RegionalEndpoint> RegionalEndpoints;
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a hardcoded list of Regional Endpoints.
        /// </summary>
        public RegionalEndpointList()
        {
            RegionalEndpoints = new List<RegionalEndpoint>
            {
                new RegionalEndpoint()
                {
                    ServiceRegion = "BR",
                    ServicePlatform = "BR1",
                    Host = "br1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "EUNE",
                    ServicePlatform = "EUN1",
                    Host = "eun1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "EUW",
                    ServicePlatform = "EUW1",
                    Host = "euw1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "JP",
                    ServicePlatform = "JP1",
                    Host = "jp1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "KR",
                    ServicePlatform = "KR",
                    Host = "kr" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "LAN",
                    ServicePlatform = "LA1",
                    Host = "la1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "LAS",
                    ServicePlatform = "LA2",
                    Host = "la2" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "NA",
                    ServicePlatform = "NA1",
                    Host = "na1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "OCE",
                    ServicePlatform = "OC1",
                    Host = "oc1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "TR",
                    ServicePlatform = "TR1",
                    Host = "tr1" + RiotSuffix,
                },

                new RegionalEndpoint()
                {
                    ServiceRegion = "RU",
                    ServicePlatform = "RU",
                    Host = "ru" + RiotSuffix,
                },
            };
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Return the host from a region.
        /// </summary>
        /// <param name="region">Region.</param>
        /// <returns>Host.</returns>
        public string GetHostFromRegion(string region)
        {
            return RegionalEndpoints
                .Single(x => x.ServiceRegion == region.ToUpper())
                .Host;
        }

        /// <summary>
        /// Return all hosts in a list of strings.
        /// </summary>
        /// <returns>
        /// List of hosts as strings.
        /// </returns>
        public List<string> GetHostList()
        {
            return RegionalEndpoints
                .Select(x => x.Host)
                .ToList();
        }

        /// <summary>
        /// A list of all endpoints.
        /// PBE excluded.
        /// </summary>
        /// <returns>
        /// List of all Regional Endpoint objects.
        /// </returns>
        public List<RegionalEndpoint> GetRegionalEndpoints()
        {
            return RegionalEndpoints
                .ToList();
        }

        /// <summary>
        /// PBE Server.
        /// </summary>
        /// <returns>
        /// PBE Server Regional Endpoint.
        /// </returns>
        public RegionalEndpoint GetPbeEndpoint()
        {
            return new RegionalEndpoint()
            {
                ServiceRegion = "PBE",
                ServicePlatform = "PBE1",
                Host = "pbe1.api.riotgames.com",
            };
        }
        #endregion
    }
}