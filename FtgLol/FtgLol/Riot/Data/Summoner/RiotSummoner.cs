﻿using Newtonsoft.Json;
namespace FtgGamingLoL.Riot.Data
{
    public class RiotSummoner

    {
        #region Properties
        /// <summary>
        /// Account ID
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// Account ID
        /// </summary>
        public string accountId { get; set; }

        /// <summary>
        /// Summoner name
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Summoner level associated with the summoner
        /// </summary>
        public long? summonerLevel { get; set; }

        /// <summary>
        /// ID of the summoner icon associated with the summoner
        /// </summary>
        public int? profileIconId { get; set; }

        /// <summary>
        /// Date summoner was last modified specified as epoch milliseconds. The following events will update this timestamp:
        /// profile icon change, playing the tutorial or advanced tutorial, finishing a game, summoner name change
        /// </summary>
        public long? revisionDate { get; set; }

        /// <summary>
        /// Date summoner was last modified specified as epoch milliseconds. The following events will update this timestamp:
        /// profile icon change, playing the tutorial or advanced tutorial, finishing a game, summoner name change
        /// </summary>
        public string region { get; set; }

        /// <summary>
        /// Figure 
        /// </summary>
        public string puuid { get; set; }
        #endregion
    }
}
