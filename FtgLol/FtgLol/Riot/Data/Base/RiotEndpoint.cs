﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FtgLol.Riot.Data.Base
{
    public class RiotEndpoint
    {
        public string Host { get; set; }
        public string Locales { get; set; }
        public string Version { get; set; }
    }
}
