﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FtgLol.Migrations
{
    public partial class afifau : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locales_Shards_HostNameString",
                table: "Locales");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Shards_HostName",
                table: "Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Shards",
                table: "Shards");

            migrationBuilder.DropIndex(
                name: "IX_Shards_HostName",
                table: "Shards");

            migrationBuilder.AlterColumn<string>(
                name: "RegionTag",
                table: "Shards",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "HostName",
                table: "Shards",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Shards",
                table: "Shards",
                column: "RegionTag");

            migrationBuilder.CreateIndex(
                name: "IX_Shards_HostName",
                table: "Shards",
                column: "HostName",
                unique: true,
                filter: "[HostName] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Locales_Shards_HostNameString",
                table: "Locales",
                column: "HostNameString",
                principalTable: "Shards",
                principalColumn: "RegionTag",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Shards_HostName",
                table: "Services",
                column: "HostName",
                principalTable: "Shards",
                principalColumn: "RegionTag",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locales_Shards_HostNameString",
                table: "Locales");

            migrationBuilder.DropForeignKey(
                name: "FK_Services_Shards_HostName",
                table: "Services");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Shards",
                table: "Shards");

            migrationBuilder.DropIndex(
                name: "IX_Shards_HostName",
                table: "Shards");

            migrationBuilder.AlterColumn<string>(
                name: "HostName",
                table: "Shards",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "RegionTag",
                table: "Shards",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Shards",
                table: "Shards",
                column: "HostName");

            migrationBuilder.CreateIndex(
                name: "IX_Shards_HostName",
                table: "Shards",
                column: "HostName",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Locales_Shards_HostNameString",
                table: "Locales",
                column: "HostNameString",
                principalTable: "Shards",
                principalColumn: "HostName",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Services_Shards_HostName",
                table: "Services",
                column: "HostName",
                principalTable: "Shards",
                principalColumn: "HostName",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
