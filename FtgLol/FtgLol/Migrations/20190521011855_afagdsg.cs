﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FtgLol.Migrations
{
    public partial class afagdsg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FtgUsers",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtgUsers", x => x.UserId);
                });

            migrationBuilder.CreateTable(
                name: "Shards",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    RegionTag = table.Column<string>(nullable: true),
                    HostName = table.Column<string>(nullable: false),
                    Slug = table.Column<string>(nullable: true),
                    ServiceRegion = table.Column<string>(nullable: true),
                    ServicePlatform = table.Column<string>(nullable: true),
                    Host = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shards", x => x.HostName);
                });

            migrationBuilder.CreateTable(
                name: "SummonerVerificationCode",
                columns: table => new
                {
                    UserName = table.Column<string>(nullable: false),
                    VerificationCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SummonerVerificationCode", x => x.UserName);
                });

            migrationBuilder.CreateTable(
                name: "FtgSummoner",
                columns: table => new
                {
                    SummonerId = table.Column<string>(nullable: true),
                    AccountId = table.Column<string>(nullable: false),
                    Puuid = table.Column<string>(nullable: true),
                    SummonerName = table.Column<string>(nullable: true),
                    SummonerLevel = table.Column<long>(nullable: true),
                    ProfileIconId = table.Column<int>(nullable: true),
                    RevisionDate = table.Column<long>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    FtgUserUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtgSummoner", x => x.AccountId);
                    table.ForeignKey(
                        name: "FK_FtgSummoner_FtgUsers_FtgUserUserId",
                        column: x => x.FtgUserUserId,
                        principalTable: "FtgUsers",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Locales",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    HostNameString = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locales", x => x.Name);
                    table.ForeignKey(
                        name: "FK_Locales_Shards_HostNameString",
                        column: x => x.HostNameString,
                        principalTable: "Shards",
                        principalColumn: "HostName",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    Slug = table.Column<string>(nullable: true),
                    HostName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => new { x.Name, x.HostName });
                    table.ForeignKey(
                        name: "FK_Services_Shards_HostName",
                        column: x => x.HostName,
                        principalTable: "Shards",
                        principalColumn: "HostName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Champions",
                columns: table => new
                {
                    Version = table.Column<string>(nullable: false),
                    Id = table.Column<string>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Blurb = table.Column<string>(nullable: true),
                    Partype = table.Column<string>(nullable: true),
                    LocaleName = table.Column<string>(nullable: false),
                    Attack = table.Column<int>(nullable: false),
                    Defense = table.Column<int>(nullable: false),
                    Magic = table.Column<int>(nullable: false),
                    Difficulty = table.Column<int>(nullable: false),
                    Hp = table.Column<double>(nullable: false),
                    HpPerLevel = table.Column<double>(nullable: false),
                    Mp = table.Column<double>(nullable: false),
                    MpPerLevel = table.Column<double>(nullable: false),
                    MoveSpeed = table.Column<double>(nullable: false),
                    Armor = table.Column<double>(nullable: false),
                    ArmorPerLevel = table.Column<double>(nullable: false),
                    SpellBlock = table.Column<double>(nullable: false),
                    SpellBlockPerLevel = table.Column<double>(nullable: false),
                    AttackRange = table.Column<double>(nullable: false),
                    HpRegen = table.Column<double>(nullable: false),
                    HpRegenPerLevel = table.Column<double>(nullable: false),
                    MpRegen = table.Column<double>(nullable: false),
                    MpRegenPerLevel = table.Column<double>(nullable: false),
                    Crit = table.Column<double>(nullable: false),
                    CritPerLevel = table.Column<double>(nullable: false),
                    AttackDamage = table.Column<double>(nullable: false),
                    AttackDamagePerLevel = table.Column<double>(nullable: false),
                    AttackSpeedOffset = table.Column<double>(nullable: false),
                    AttackSpeedPerLevel = table.Column<double>(nullable: false),
                    Full = table.Column<string>(nullable: true),
                    Sprite = table.Column<string>(nullable: true),
                    Group = table.Column<string>(nullable: true),
                    X = table.Column<int>(nullable: false),
                    Y = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Champions", x => new { x.Id, x.LocaleName, x.Version });
                    table.ForeignKey(
                        name: "FK_Champions_Locales_Name",
                        column: x => x.Name,
                        principalTable: "Locales",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    LocaleName = table.Column<string>(nullable: false),
                    Version = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Colloq = table.Column<string>(nullable: true),
                    PlainText = table.Column<string>(nullable: true),
                    BaseCost = table.Column<int>(nullable: false),
                    Purchasable = table.Column<bool>(nullable: false),
                    Total = table.Column<int>(nullable: false),
                    Sell = table.Column<int>(nullable: false),
                    FlatHPPoolMod = table.Column<int>(nullable: false),
                    RFlatHPModPerLevel = table.Column<int>(nullable: false),
                    FlatMPPoolMod = table.Column<int>(nullable: false),
                    RFlatMPModPerLevel = table.Column<int>(nullable: false),
                    PercentHPPoolMod = table.Column<int>(nullable: false),
                    PercentMPPoolMod = table.Column<int>(nullable: false),
                    FlatHPRegenMod = table.Column<int>(nullable: false),
                    RFlatHPRegenModPerLevel = table.Column<int>(nullable: false),
                    PercentHPRegenMod = table.Column<int>(nullable: false),
                    FlatMPRegenMod = table.Column<int>(nullable: false),
                    RFlatMPRegenModPerLevel = table.Column<int>(nullable: false),
                    PercentMPRegenMod = table.Column<int>(nullable: false),
                    FlatArmorMod = table.Column<int>(nullable: false),
                    RFlatArmorModPerLevel = table.Column<int>(nullable: false),
                    PercentArmorMod = table.Column<int>(nullable: false),
                    RFlatArmorPenetrationMod = table.Column<int>(nullable: false),
                    RFlatArmorPenetrationModPerLevel = table.Column<int>(nullable: false),
                    RPercentArmorPenetrationMod = table.Column<int>(nullable: false),
                    RPercentArmorPenetrationModPerLevel = table.Column<int>(nullable: false),
                    FlatPhysicalDamageMod = table.Column<int>(nullable: false),
                    RFlatPhysicalDamageModPerLevel = table.Column<int>(nullable: false),
                    PercentPhysicalDamageMod = table.Column<int>(nullable: false),
                    FlatMagicDamageMod = table.Column<int>(nullable: false),
                    RFlatMagicDamageModPerLevel = table.Column<int>(nullable: false),
                    PercentMagicDamageMod = table.Column<int>(nullable: false),
                    FlatMovementSpeedMod = table.Column<int>(nullable: false),
                    RFlatMovementSpeedModPerLevel = table.Column<int>(nullable: false),
                    PercentMovementSpeedMod = table.Column<int>(nullable: false),
                    RPercentMovementSpeedModPerLevel = table.Column<int>(nullable: false),
                    FlatAttackSpeedMod = table.Column<int>(nullable: false),
                    PercentAttackSpeedMod = table.Column<int>(nullable: false),
                    RPercentAttackSpeedModPerLevel = table.Column<int>(nullable: false),
                    RFlatDodgeMod = table.Column<int>(nullable: false),
                    RFlatDodgeModPerLevel = table.Column<int>(nullable: false),
                    PercentDodgeMod = table.Column<int>(nullable: false),
                    FlatCritChanceMod = table.Column<int>(nullable: false),
                    RFlatCritChanceModPerLevel = table.Column<int>(nullable: false),
                    PercentCritChanceMod = table.Column<int>(nullable: false),
                    FlatCritDamageMod = table.Column<int>(nullable: false),
                    RFlatCritDamageModPerLevel = table.Column<int>(nullable: false),
                    PercentCritDamageMod = table.Column<int>(nullable: false),
                    FlatBlockMod = table.Column<int>(nullable: false),
                    PercentBlockMod = table.Column<int>(nullable: false),
                    FlatSpellBlockMod = table.Column<int>(nullable: false),
                    RFlatSpellBlockModPerLevel = table.Column<int>(nullable: false),
                    PercentSpellBlockMod = table.Column<int>(nullable: false),
                    FlatEXPBonus = table.Column<int>(nullable: false),
                    PercentEXPBonus = table.Column<int>(nullable: false),
                    RPercentCooldownMod = table.Column<int>(nullable: false),
                    RPercentCooldownModPerLevel = table.Column<int>(nullable: false),
                    RFlatTimeDeadMod = table.Column<int>(nullable: false),
                    RFlatTimeDeadModPerLevel = table.Column<int>(nullable: false),
                    RPercentTimeDeadMod = table.Column<int>(nullable: false),
                    RPercentTimeDeadModPerLevel = table.Column<int>(nullable: false),
                    RFlatGoldPer10Mod = table.Column<int>(nullable: false),
                    RFlatMagicPenetrationMod = table.Column<int>(nullable: false),
                    RFlatMagicPenetrationModPerLevel = table.Column<int>(nullable: false),
                    RPercentMagicPenetrationMod = table.Column<int>(nullable: false),
                    RPercentMagicPenetrationModPerLevel = table.Column<int>(nullable: false),
                    FlatEnergyRegenMod = table.Column<int>(nullable: false),
                    RFlatEnergyRegenModPerLevel = table.Column<int>(nullable: false),
                    FlatEnergyPoolMod = table.Column<int>(nullable: false),
                    RFlatEnergyModPerLevel = table.Column<int>(nullable: false),
                    PercentLifeStealMod = table.Column<int>(nullable: false),
                    PercentSpellVampMod = table.Column<int>(nullable: false),
                    IsRune = table.Column<bool>(nullable: false),
                    Tier = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Full = table.Column<string>(nullable: true),
                    Sprite = table.Column<string>(nullable: true),
                    Group = table.Column<string>(nullable: true),
                    X = table.Column<int>(nullable: false),
                    Y = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => new { x.Id, x.LocaleName, x.Version });
                    table.ForeignKey(
                        name: "FK_Items_Locales_LocaleName",
                        column: x => x.LocaleName,
                        principalTable: "Locales",
                        principalColumn: "Name",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Incidents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Created = table.Column<string>(nullable: true),
                    ServiceHostName = table.Column<string>(nullable: true),
                    ServiceName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Incidents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Incidents_Services_ServiceName_ServiceHostName",
                        columns: x => new { x.ServiceName, x.ServiceHostName },
                        principalTable: "Services",
                        principalColumns: new[] { "Name", "HostName" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FtgChampionTag",
                columns: table => new
                {
                    Tag = table.Column<string>(nullable: false),
                    FtgChampionId = table.Column<string>(nullable: true),
                    FtgChampionLocaleName = table.Column<string>(nullable: true),
                    FtgChampionVersion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtgChampionTag", x => x.Tag);
                    table.ForeignKey(
                        name: "FK_FtgChampionTag_Champions_FtgChampionId_FtgChampionLocaleName_FtgChampionVersion",
                        columns: x => new { x.FtgChampionId, x.FtgChampionLocaleName, x.FtgChampionVersion },
                        principalTable: "Champions",
                        principalColumns: new[] { "Id", "LocaleName", "Version" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FtgItemTag",
                columns: table => new
                {
                    Tag = table.Column<string>(nullable: false),
                    FtgItemId = table.Column<string>(nullable: true),
                    FtgItemLocaleName = table.Column<string>(nullable: true),
                    FtgItemVersion = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FtgItemTag", x => x.Tag);
                    table.ForeignKey(
                        name: "FK_FtgItemTag_Items_FtgItemId_FtgItemLocaleName_FtgItemVersion",
                        columns: x => new { x.FtgItemId, x.FtgItemLocaleName, x.FtgItemVersion },
                        principalTable: "Items",
                        principalColumns: new[] { "Id", "LocaleName", "Version" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Severity = table.Column<string>(nullable: true),
                    Author = table.Column<string>(nullable: true),
                    Created = table.Column<string>(nullable: true),
                    Updated = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    IncidentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Messages_Incidents_IncidentId",
                        column: x => x.IncidentId,
                        principalTable: "Incidents",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Translations",
                columns: table => new
                {
                    LocaleName = table.Column<string>(nullable: false),
                    Content = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<string>(nullable: true),
                    MessageId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Translations", x => new { x.LocaleName, x.MessageId });
                    table.ForeignKey(
                        name: "FK_Translations_Messages_MessageId",
                        column: x => x.MessageId,
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Champions_Name",
                table: "Champions",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_FtgChampionTag_FtgChampionId_FtgChampionLocaleName_FtgChampionVersion",
                table: "FtgChampionTag",
                columns: new[] { "FtgChampionId", "FtgChampionLocaleName", "FtgChampionVersion" });

            migrationBuilder.CreateIndex(
                name: "IX_FtgItemTag_FtgItemId_FtgItemLocaleName_FtgItemVersion",
                table: "FtgItemTag",
                columns: new[] { "FtgItemId", "FtgItemLocaleName", "FtgItemVersion" });

            migrationBuilder.CreateIndex(
                name: "IX_FtgSummoner_FtgUserUserId",
                table: "FtgSummoner",
                column: "FtgUserUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Incidents_ServiceName_ServiceHostName",
                table: "Incidents",
                columns: new[] { "ServiceName", "ServiceHostName" });

            migrationBuilder.CreateIndex(
                name: "IX_Items_LocaleName",
                table: "Items",
                column: "LocaleName");

            migrationBuilder.CreateIndex(
                name: "IX_Locales_HostNameString",
                table: "Locales",
                column: "HostNameString");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_IncidentId",
                table: "Messages",
                column: "IncidentId");

            migrationBuilder.CreateIndex(
                name: "IX_Services_HostName",
                table: "Services",
                column: "HostName");

            migrationBuilder.CreateIndex(
                name: "IX_Shards_HostName",
                table: "Shards",
                column: "HostName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Translations_MessageId",
                table: "Translations",
                column: "MessageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FtgChampionTag");

            migrationBuilder.DropTable(
                name: "FtgItemTag");

            migrationBuilder.DropTable(
                name: "FtgSummoner");

            migrationBuilder.DropTable(
                name: "SummonerVerificationCode");

            migrationBuilder.DropTable(
                name: "Translations");

            migrationBuilder.DropTable(
                name: "Champions");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "FtgUsers");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "Locales");

            migrationBuilder.DropTable(
                name: "Incidents");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "Shards");
        }
    }
}
